using HelloFunction;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HelloFunction_UnitTest2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            // Program prg = new Program();

            // Act 
            var result = Program.getString();

            // Assert
            Assert.AreEqual(result, "HelloFunction");
        }
    }
}
